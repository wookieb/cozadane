var connect = require('connect'),
    httpProxy = require('http-proxy');

var server = connect(),
    proxy = httpProxy.createProxyServer({}),
    forwardTo = process.argv[2] || 'http://192.168.0.11:9000';

var isRequestForProxy = function(request) {
    return request.url.substr(0, 5) === '/rest' ||
        request.url.substr(0, 2) === '/@';
};

proxy.on('error', function(err) {
    console.error('Proxy error: ' + err.message);
});

server.use(function(req, res, next) {
    if (isRequestForProxy(req)) {
        proxy.web(req, res, {target: forwardTo});
    } else {
        next();
    }
});

server.use(connect.static(__dirname));
server.use(function onerror(err, req, res, next) {
    console.error('Error: ' + err.message);
});

server.listen(3000, function() {
    console.log('Listening on: http://localhost:3000');
    console.log('Rest forwarded to: ' + forwardTo);
});

