'use strict';

var gulp = require('gulp'),
    serve = require('gulp-serve'),
    httpProxy = require('http-proxy');


var isRequestForProxy = function(request) {
    return request.url.substr(0, 5) === '/rest' ||
            request.url.substr(0, 2) === '/@';
};

gulp.task('serve', function(done) {
    var proxy = httpProxy.createProxyServer({});;
    serve({
        root: __dirname,
        middleware: function(req, res, next) {
            if (isRequestForProxy(req)) {
                proxy.web(req, res, {target: 'http://192.168.0.11:9000'});
            } else {
                next();
            }
        }
    })(done);
});

