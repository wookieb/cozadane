App.Model.Note = Backbone.Model.extend({
    urlRoot: '/rest/note',
    // TODO extra: dodaj pole Subject (przedmiot szkolny)
    defaults: {
        title: '',
        content: ''
    },
    parse: function(data) {
        if (data.date) {
            data.date = new Date(data.date);
        }
        return data;
    }
});