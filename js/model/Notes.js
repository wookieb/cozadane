App.Model.Notes = Backbone.Collection.extend({
    url: '/rest/note/all',
    model: App.Model.Note
});