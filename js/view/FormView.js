App.View.FormView = Backbone.View.extend({
    events: {
        'submit': 'saveNote'
    },
    tagName: 'form',
    initialize: function() {
        if (!this.model) {
            this.model = new App.Model.Note();
        }
    },
    render: function() {
        // TODO: Dodaj przycisk anuluj
        var template = this.getTemplate();
        this.$el.html(template(
            this.getTemplateData()
        ));
        return this;
    },
    getTemplate: function() {
        return _.template($('#note-form-template').html());
    },
    getTemplateData: function() {

        var pad = function(num) {
            num = String(num);
            if (num.length === 1) {
                return '0'+num;
            }
            return num;
        };
        var data = this.model.toJSON(),
            date = data.date;
        data.formDate = '';
        if (date) {
            data.formDate = date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate());
        }
        return data;
    },
    saveNote: function(e) {
        e.preventDefault();

        var formData = this.$el.serializeArray(),
            model = this.model;

        formData.forEach(function(field) {
            model.set(field.name, field.value);
        });

        model.save(undefined, {
            success: function() {
                // TODO: Pokaż element w menu
                App.router.navigate('note/' + model.id, {trigger: true});
            }
        });
    }
})