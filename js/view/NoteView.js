App.View.NoteView = Backbone.View.extend({
    render: function() {
        var template = this.getTemplate();
        this.$el.html(template(this.model.attributes));
        return this;
    },
    getTemplate: function() {
        return _.template($('#note-template').html());
    }
});