App.View.MenuView = Backbone.View.extend({
    initialize: function() {
        this.listenTo(this.collection, 'sync', this.render);
        this.listenTo(this.collection, 'add', this.render);
    },
    render: function() {
        // TODO EXTRA: Wyświetl pogrupowane dane
        // TODO EXTRa: Nie wyświetlaj starszych notatek
        var template = this.getTemplate();
        var html = this.collection.map(function(note) {
            return template(note.attributes);
        }).join('');

        this.$el.html(html);
    },
    setSelected: function(model) {
        this.$el.find('.active').removeClass('active');
        if (model) {
            this.$el.find('[data-id="' + model.id + '"]').addClass('active');
        }
    },
    getTemplate: function() {
        return _.template($('#menu-entry-template').html());
    }
});