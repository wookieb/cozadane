var useView = function(view) {
    view.render();

    var $content = $('.details-content');
    $content.html('');
    $content.append(view.el);
}
App.Router = Backbone.Router.extend({
    routes: {
        'addNote': 'addNote',
        'editNote/:id': 'editNote',
        'note/:id': 'showNote',
        '*path': 'showPlaceholder'
    },
    showNote: function(id) {
        var note = App.notes.get(id);
        if (!note) {
            return;
        }

        App.menu.setSelected(note);

        useView(new App.View.NoteView({
            model: note
        }));
    },
    addNote: function() {
        useView(new App.View.FormView());
    },
    editNote: function(id) {
        var note = App.notes.get(id);
        if (!note) {
            return;
        }
        useView(new App.View.FormView({
            model: note
        }));
    },
    showPlaceholder: function() {
        useView(new App.View.PlaceholderView());
    }
});