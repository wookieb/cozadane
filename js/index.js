$(document).ready(function() {
    App.notes = new App.Model.Notes();
    App.menu = new App.View.MenuView({
        el: $('.schedule ul').get(0),
        collection: App.notes
    });


    // Wystartuj routing
    App.router = new App.Router();

    Backbone.history.start();

    App.router.navigate('/');

    // ściągnij wszystkie notatki
    App.notes.fetch();
});